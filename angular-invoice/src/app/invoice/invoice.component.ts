import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as jsPDF from 'jspdf';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas'; 
import * as $ from 'jquery'
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  isPrinting = false;
  data:any =[
    {
      RoomType : 'MBR',
      Product : 'Wardrobe',
      Model : 'slidedoor',
      UValue: '1500',
      w: '8',
      h:'6',
      cost: '72000',},
      
  ]
  constructor() { }
  
 public print(){
  // var data = document.getElementById('contentToview'); 
  window.print()
 }
 
  
  public downloadpdf()  
  {  
    var data = document.getElementById('contentToview');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('quotation.pdf'); // Generated PDF   
    });  
  }  
  
  ngOnInit() {
    $(document).ready(function(){
      var i :any=1;
      $("#add_row").click(function(){ var b=i-1;
          $('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html( i+1);
          $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
          i++; 
      });
      $("#delete_row").click(function(){
        if(i>1){
      $("#addr"+(i-1)).html('');
      i--;
      }
      calc();
    });
    
    $('#tab_logic tbody').on('keyup change',function(){
      calc();
    });
    $('#tax').on('keyup change',function(){
     calc_total();
    });
    $('#mis_amt').on('keyup change',function(){
      debounceTime(2000)
      calc_total();
    },)
  
  });
  }

}
function calc()
{
	$('#tab_logic tbody tr').each(function(i, element) {
		var html = $(this).html();
		if(html!='')
		{
      var width:any = $(this).find('.width').val();
      var height:any=$(this).find('.height').val();
      var price: any = $(this).find('.price').val();
      
			$(this).find('.total').val(width*height*price);
			
			calc_total();
		}
    });
}

function calc_total()
{
     var total:any=0;
     var misamt;
	$('.total').each(function() {
        var value:any = $(this).val();
        total += parseInt(value);
    });
    $('#mis_amt').each(function(){
      var amount:any = $(this).val();
      misamt = parseInt(amount);
    })
	$('#sub_total').val(total.toFixed(2));
  var tax_val:any=$('#tax').val();
  
  var tax_sum : any =total/100*tax_val ;
  
  
	$('#tax_amount').val(tax_sum.toFixed(2));
  $('#total_amount').val((tax_sum+total+misamt));
 
}

